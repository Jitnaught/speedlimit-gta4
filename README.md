Have you ever wanted the cops to be more realistic? Well then this is the mod for you!
This mod makes it so when you go at least 10 mph over the speed limit, you get a 1 star wanted level! And when you go at least 25 mph over the limit, you get a 2 star wanted level! Isn't that realistic!?

How to install:
Install an ASI Loader (YAASIL recommended) and .NET Scripthook.
Copy SpeedLimit.net.dll and SpeedLimit.ini to the scripts folder in your GTA IV/EFLC directory.

Controls:
Press Right-CTRL + L to toggle the speed limit. It is enabled on startup.

What you can change in the INI file:
The keys for toggling the mod.
The distance at which cops can see you.
The speeds at which you get the 1 star and 2 star wanted levels.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
