﻿using GTA;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpeedLimit
{
    public class SpeedLimit : Script
    {
        private bool enabled = true;
        private float averageSpeed = 10;

        private bool useHoldKey;
        private Keys holdKey, pressKey;
        private UInt16 speedAboveLimit1, speedAboveLimit2;
        private float distanceCops;

        public SpeedLimit()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("use_hold_key", "keys", true);
                Settings.SetValue("hold_key", "keys", Keys.RControlKey);
                Settings.SetValue("press_key", "keys", Keys.L);
                Settings.SetValue("distance_when_cops_see_you", "settings", 75);
                Settings.SetValue("speed_above_limit_to_get_1star", "settings", 10);
                Settings.SetValue("speed_above_limit_to_get_2star", "settings", 25);
                Settings.Save();
            }

            useHoldKey = Settings.GetValueBool("use_hold_key", "keys", true);
            holdKey = Settings.GetValueKey("hold_key", "keys", Keys.RControlKey);
            pressKey = Settings.GetValueKey("press_key", "keys", Keys.L);
            distanceCops = Settings.GetValueFloat("distance_when_cops_see_you", "settings", 75);
            speedAboveLimit1 = (UInt16)Settings.GetValueInteger("speed_above_limit_to_get_1star", "settings", 10);
            speedAboveLimit2 = (UInt16)Settings.GetValueInteger("speed_above_limit_to_get_2star", "settings", 25);

            KeyDown += SpeedLimit_KeyDown;
            Interval = 1000;
            Tick += AverageSpeedTick;
            new GTA.Timer(500, true).Tick += SpeedLimitTick;
        }

        private int GetWantedLevelToSet(float speed, float speedLimit)
        {
            float difference = Math.Abs(speed - speedLimit);
            if (difference >= speedAboveLimit1 && difference < speedAboveLimit2) return 1;
            else if (difference >= speedAboveLimit2) return 2;
            return 0;
        }

        private bool IsAboveSpeedLimit(float speed)
        {
            if (speed >= speedAboveLimit1 || speed >= speedAboveLimit2) return true;
            return false;
        }

        private void SpeedLimitTick(object sender, EventArgs e)
        {
            if (enabled && (Player.WantedLevel == 0 || Player.WantedLevel == 1) && Player.Character.isInVehicle() && IsAboveSpeedLimit(Player.Character.CurrentVehicle.Speed))
            {
                Ped[] closePeds = World.GetPeds(Player.Character.Position, distanceCops);
                foreach (Ped ped in closePeds)
                {
                    if (ped.Model == Model.BasicCopModel || ped.Model == Model.CurrentCopModel && Math.Abs(Player.Character.Position.Z - ped.Position.Z) <= 20)
                    {
                        int wantedLevel = GetWantedLevelToSet(Player.Character.CurrentVehicle.Speed, averageSpeed);
                        if (wantedLevel > 0 && wantedLevel > Player.WantedLevel) Player.WantedLevel = wantedLevel;
                        break;
                    }
                }
            }
        }

        private void AverageSpeedTick(object sender, EventArgs e)
        {
            if (enabled && (Player.WantedLevel == 0 || Player.WantedLevel == 1))
            {
                Vehicle[] closeVehs = World.GetVehicles(Player.Character.Position, 500);
                if (closeVehs.Length > 0 && !(closeVehs.Length == 1 && Player.Character.isInVehicle() && closeVehs[0] == Player.Character.CurrentVehicle))
                {
                    float total = 0;
                    UInt16 amount = 0;
                    foreach (Vehicle veh in closeVehs)
                    {
                        if (veh != null && veh.Health > 0 && veh.isAlive && veh.EngineRunning && !(Player.Character.isInVehicle() && veh == Player.Character.CurrentVehicle) && veh.Speed > 5)
                        {
                            total += veh.Speed;
                            amount++;
                        }
                    }
                    if (total != 0 && amount != 0) averageSpeed = total / amount;
                }
            }
        }

        private void SpeedLimit_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if ((!useHoldKey || isKeyPressed(holdKey)) && e.Key == pressKey)
            {
                enabled = !enabled;
                Game.DisplayText("Speed limit now " + (enabled ? "enabled" : "disabled"));
            }
        }
    }
}
